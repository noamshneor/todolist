import { ITask } from "../interfaces/todo.interfaces.js";
import { generateID } from "../utils/generate-id.js";

export function create(tasks: ITask[], title: string) {
  const todotask: ITask = { id: generateID(), text: title, isDone: false };
  tasks.push(todotask);
  return tasks;
}

export function read(tasks: ITask[], arg: "all" | "complete" | "incomplete") {
  switch (arg) {
    case "all":
      return tasks;
    case "complete":
      return tasks.filter((item) => item.isDone);
    case "incomplete":
      return tasks.filter((item) => !item.isDone);
  }
}

export function update(tasks: ITask[], id: string) {
  for (let i = 0; i < tasks.length; i++) {
    if (tasks[i].id === id) {
      tasks[i].isDone = !tasks[i].isDone;
      break;
    }
  }
  return tasks;
}

export function del(tasks: ITask[], id: string) {
  for (let i = 0; i < tasks.length; i++) {
    if (tasks[i].id === id) {
      tasks.splice(i, 1);
      break;
    }
  }
  return tasks;
}
