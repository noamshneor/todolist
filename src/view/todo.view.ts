import { ITask } from "../interfaces/todo.interfaces.js";

export function printTasks(tasks: ITask[]) {
  console.log("TODO LIST");
  for (const task of tasks) {
    console.log(`id: ${task.id}, text: ${task.text}, isDone: ${task.isDone}`);
  }
}

export function printError() {
  console.log("Sorry, that is not something I know how to do.");
}

export function printHelp() {
  console.log(`READ ME

            CREATE   -->
                    description: create task with this text
                    params:  -t | --title
                    usage: npm run mydev2 create -t="buy milk"

            READ     -->
                    description: print the task with this id
                    params:  -a | --all | -o | --open | -c | --completed
                    usage: npm run mydev2 read -o
                    usage: npm run mydev2 read --all

            UPDATE   -->
                    description: update v-done/x-not done
                    params:  -id | -d | --done
                    usage: npm run mydev2 update -id=2 -d="v"

            DELETE   -->
                    description: delete the task with this id
                    params:  -id
                    usage: npm run mydev2 delete -id=2

            HELP     -->
                    description: prints readme
                    params:
                    usage: npm run mydev2 help`);
}

// console.log("READ ME");
// console.log("CREATE   -->   input: create task_title       (create task with this text)");
// console.log("READ     -->   input: read id_number          (print the task with this id)");
// console.log("UPDATE   -->   input: update id_number        (update state to opposite) ");
// console.log("DELETE   -->   input: delete id_number        (delete the task with this id)");
// console.log("PRINT    -->   input: print                   (prints all the todo tasks)");
