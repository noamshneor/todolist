import fs from "fs/promises";
import { ITask } from "../interfaces/todo.interfaces.js";

export function saveToDB(pathDB: string, tasks: ITask[]) {
  fs.writeFile(pathDB, JSON.stringify(tasks), "utf-8");
}

export async function checkFile(pathDB: string) {
  try {
    const file = JSON.parse(await fs.readFile(pathDB, "utf-8"));
    return file;
  } catch (error) {
    fs.writeFile(pathDB, JSON.stringify([]), "utf-8"); // if file not exists
    return [];
  }
}
